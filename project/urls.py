from django.urls import path
from project import views
from project.views import ProjectAPIListCreate
urlpatterns=[
    path(r"projects/", ProjectAPIListCreate.as_view(), name="Project"),
]