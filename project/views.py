from django.shortcuts import render
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from task.models import *
from project.serializers import *
from project.models import Project


class ProjectAPIListCreate(generics.ListCreateAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = [AllowAny]

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = ProjectSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        data = request.data
        new_project = Project.objects.create(id=data["id"], projectName=data["projectName"])

        new_project.save()

        for task in data["tasks"]:
            # Task_object = Task.objects.get(description=task["description"])
            # new_project.tasks.add(task_object)
            new_task = Task.objects.create(id=task["id"], description=task["description"], completed=task["completed"])
            new_task.save()
            new_project.tasks.add(new_task)

        serializer = ProjectSerializer(new_project)

        return Response(serializer.data)

    def put(self, request, *args, **kwargs):
        data = request.data
        project_object = Project.objects.get(id = data["id"])

        for task in data["tasks"]:
            new_task = Task.objects.create(id=task["id"], description=task["description"], completed=task["completed"])
            new_task.save()
            project_object.tasks.add(new_task)



        #project_object.tasks = data["tasks"]

        project_object.save()

        serializer = ProjectSerializer(project_object)
        return Response(serializer.data)
