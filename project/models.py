from django.db import models
import uuid

from task.models import Task


# Create your models here.
class Project(models.Model):
    class Meta:
        verbose_name = " project"
        verbose_name_plural = "projects"
        db_table = " itw_project"

    id = models.UUIDField(primary_key=True, editable=False, unique=True, default=uuid.uuid4)
    projectName = models.CharField(max_length=500)
    tasks = models.ManyToManyField(Task)

    def __str__(self):
        return self.projectName
