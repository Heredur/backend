from rest_framework import serializers
from project.models import *
from task.serializers import TaskSerializer


class ProjectSerializer(serializers.ModelSerializer):
    #task_list = serializers.RelatedField(many=True, read_only= True)

    class Meta:
        model = Project
        fields = ["id", "projectName", "tasks", ]
        depth = 1