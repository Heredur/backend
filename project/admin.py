from django.contrib import admin

# Register your models here.
import project.models

admin.site.register(project.models.Project)