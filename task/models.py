from django.db import models
import uuid


# Create your models here.
class Task(models.Model):
    class Meta:
        verbose_name = " description"
        verbose_name_plural = "descriptions"
        db_table = " itw_description"

    id = models.UUIDField(primary_key=True, editable=False, unique=True, default=uuid.uuid4)
    description = models.CharField(max_length=500)
    completed = models.BooleanField(default=False)

    # project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="task_list")

    def __str__(self):
        return self.description

    def __unicode__(self):
        return '%s' % self.description

    # class Completed(models.model):
    #     class Meta:
    #         verbose_name = " completed"
    #         verbose_name_plural = " ccmpleted"
    #         db_table = "itw_completed"
    #
    #     completed = models.BooleanField(default=False)
