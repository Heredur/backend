# noinspection PyUnresolvedReferences
from django.urls import path

from task.views import TaskAPIListCreate, TaskAPIUpdate

urlpatterns = [path(r"tasks/", TaskAPIListCreate.as_view(), name="Task"),
               path(r"tasks/(?P<id>.*)/", TaskAPIUpdate.as_view(), name="update"), ]
